//
//  main.c
//  he_tools
//
//  Created by Sid on 27/09/13.
//  Copyright (c) 2013 whackylabs. All rights reserved.
//

#include <stdio.h>

#include "Filenames.h"

int main(int argc, char **argv) {
 if (argc != 3) {
  printf("Usage: %s <out directory name> <in directory name>\n",argv[0]);
  return 0;
 }
 
 ObfusContext context;
 ObfusContext_Create(&context, argv[1], "final_assets.h");
 ObfuscateAssets(&context, argv[2]);
 ObfusContext_Release(&context);
 
 return 0;
}
