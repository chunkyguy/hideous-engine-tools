/**
 See README.txt
 */
#include "Filenames.h"

#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>

#define MAX_PATH 1024

// Test whether the filename is to be obfuscated.
static bool IsObfuscateType(const char *filename_ro) {
 char *extn;
 char *obfuscate_types[] = {
  "png", "jpg", "xml", "vertsh", "fragsh"
 };
 size_t count = sizeof(obfuscate_types)/sizeof(obfuscate_types[0]);
 char filename[MAX_LEN];
 
 
 extn = Split(strcpy(filename, filename_ro));
 for (int i = 0; i < count; ++i) {
  if (strcmp(extn, obfuscate_types[i]) == 0) {
   return true;
  }
 }
 return false;
}

// copy file and save in header using context
static void Obfuscate(ObfusContext *context, const char *filepath, const char *filename) {
 char newfilename[MAX_LEN];
 char newfilepath[MAX_PATH];
 char filename_copy[MAX_LEN];
 char *filename_extn;
 
 strcpy(filename_copy, filename);
 filename_extn = Split(filename_copy);
 /* Create new filename */
 sprintf(newfilename, "File%d",context->file_num++);
 fprintf(context->file, "#define %s_%s %s\n", filename_copy, filename_extn, newfilename);
 CopyFile(GetAbsolutePath(newfilepath, context->out_dir, newfilename), filepath);
}


void CopyFile(const char *dest, const char *src) {
 if (fork() == 0) {
  execl("/bin/cp", "/bin/cp/", src, dest, NULL);
 }
}


char *Split(char *filename) {
 char *extn = strrchr(filename, '.');
 if (extn != NULL) {
  *extn++ = '\0';
 }
 
 return extn;
}

char *GetAbsolutePath(char *abs_path, const char *dir, const char *file) {
 strcpy(abs_path, dir);
 if (dir[strlen(dir)-1] != '/') {
  strcat(abs_path, "/");
 }
 strcat(abs_path, file);
 return abs_path;
}

void ObfuscateAssets(ObfusContext *context, const char *dir_path) {
 char filepath[MAX_PATH];
 DIR *dp;
 struct dirent *ep;

 /* Open current directory*/
 if ((dp = opendir(dir_path)) == NULL) {
  printf("Unable to open directory: %s\n",dir_path);
  return ;
 }
 
 while ((ep = readdir(dp)) != NULL) {
  /* Copy absolute path of the file being read */
  GetAbsolutePath(filepath, dir_path, ep->d_name);

  if (ep->d_type == DT_DIR && (ep->d_name[0] != '.')) {
   ObfuscateAssets(context, filepath);
  } else if (ep->d_type == DT_REG && IsObfuscateType(ep->d_name)) {
   Obfuscate(context, filepath, ep->d_name);
  }
 }
 closedir(dp);
}

bool ObfusContext_Create(ObfusContext *context,  const char *out_dir, const char *out_file) {
 char filename[MAX_LEN];
 
 context->file = fopen(out_file, "w");
 if (!context->file) {
  return false;
 }

 /* Get first part of the out_file */
 strcpy(filename, out_file);
 Split(filename);
 fprintf(context->file,
         "/* Auto created do not edit*/\n\n"
         "#ifndef he_tools__%s_h\n"
         "#define he_tools__%s_h\n\n",filename,filename);
 context->file_num = 0;
 strcpy(context->out_dir, out_dir);
 return true;
}

void ObfusContext_Release(ObfusContext *context) {
 fprintf(context->file, "\n#endif\n");
 fclose(context->file);
}

