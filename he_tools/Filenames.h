//
//  Filenames.h
//  he_tools
//
//  Created by Sid on 27/09/13.
//  Copyright (c) 2013 whackylabs. All rights reserved.
//


#ifndef he_tools__filenames_h
#define he_tools__filenames_h

#include <stdio.h>
#include <stdbool.h>

#define MAX_LEN 256 // filename length

/**
  Object to obfuscate file of a directory and create a mapping file.
 */
typedef struct  {
 FILE *file;
 int file_num;
 char out_dir[MAX_LEN];
} ObfusContext;

/**
  Create a ObfusContext object.
  @param context The ObfusContext object.
  @param out_dir The output directory where all the final assets are going to be copied.
  @param out_file The mapping file name.
  @return true if all goes well.
 */
bool ObfusContext_Create(ObfusContext *context,  const char *out_dir, const char *out_file);

/**
  Commit all the final operations
  @param context The ObfusContext object.
 */
void ObfusContext_Release(ObfusContext *context);

/*
 Split a filename into two parts, file and extension
 sample.png = {sample, png}
 The result is store in the filename.
 The filename points to the filename part ('sample') and the returned pointer to extension ('png')

 @param filename The full filename
 @return If sucessful a pointer to the extension part, else NULL
 */
char *Split(char *filename);

/**
 for-each file in dir_path
  if file is directory
   ObfuscateAssets
  elif file is regular
   Obfuscate
 
 Obfuscate all regular files. Recurse for any directory.
 @param context The ObfusContext object.
 @param dir_path The directory.
 */
void ObfuscateAssets(ObfusContext *context, const char *dir_path);

/**
 abs_path = dir + file

 Create absolute path out of a directory and a file.
 @param abs_path The final path.
 @param dir The directory.
 @param file The filename
 @return abs_path.
 */
char *GetAbsolutePath(char *abs_path, const char *dir, const char *file);

/**
 dest = src.
 
 Copy file.
 */
void CopyFile(const char *dest, const char *src);
#endif
